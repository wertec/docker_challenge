import docker
import json
import threading
import requests
import os


SKIP_VAR = os.getenv('VARIABLES')
ENV_LOG_VARS = SKIP_VAR.split(',')

def update_json():
    json_out = {'containers': []}
    client = docker.APIClient()
    daemon = docker.from_env()
    containers = daemon.containers.list()

    for container in containers:
        try:
            command = client.exec_create(container=container.name, cmd='env')
        except requests.exceptions.HTTPError:
            print("Contaier has already died")
            return
        out = client.exec_start(command["Id"]).decode("utf-8").split("\n")
        if "exec failed" not in out:
            log_vars =[]
            for env_vars in out:
                for log_var in ENV_LOG_VARS:
                    if log_var in env_vars:
                        if SKIP_VAR not in env_vars:  # Check for self
                            log_vars.append(env_vars[env_vars.find("=")+1:])
                            if log_var == "VIRTUAL_HOST":
                                print("Attempting to get","http://certbot:5000/cert/" + env_vars[env_vars.find("=")+1:])
                                response = requests.get("http://certbot:5000/cert/" + env_vars[env_vars.find("=")+1:])
            for i in range(len(log_vars), len(ENV_LOG_VARS)):
                log_vars.append("")

            json_temp = {'container_id': container.id}
            json_temp.update({ENV_LOG_VARS[i]: log_vars[i] for i in range(len(ENV_LOG_VARS))})
            json_out['containers'].append(json_temp)

    with open('/log/data.json', 'w') as outfile:
        json.dump(json_out, outfile)

    return


def event_observer():
    client = docker.APIClient()
    for event in client.events(decode=True):
        if event['Type'] == 'container':
            if event['status'] == 'start':
                update_thread = threading.Thread(target=update_json(), args=())
                update_thread.start()
            if event['status'] == 'die':
                update_thread = threading.Thread(target=update_json(), args=())
                update_thread.start()


event_observer()

