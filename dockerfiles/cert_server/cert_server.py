from flask import Flask
import os

app = Flask(__name__)
MY_IP = os.getenv('MY_IP')


@app.route('/cert/<domain>')
def get_cert(domain):
    command = "certbot certonly --webroot --webroot-path=/var/www/html --email generic.email@gmail.com" \
              " --agree-tos --force-renewal --staging -d " + domain + "." + str(MY_IP) + ".xip.io"
    os.system(command)
    print("Got certificate for {}!".format(domain))
    return "Got certificate for {}!".format(domain)

if __name__ == '__main__':
    app.run(host='0.0.0.0')
