# Auto Certbot

This is a small application that uses Certbot to automatically get certificates for all running docker containers with VIRTUAL_HOST set. 

---

## Requirements

**Docker** and **docker-compose** must be installed.

---

## Usage

1.  In the **vars.env** list the ENVIRONMENT variables that must be gathered. Note that if VIRTUAL_HOST is not included, Certbot will **not** work.
2.  Set your public IP address in the docker-compose file.

```
docker-compose up
```

To use only the variable scraper, remove the certbot and webserver services from the docker-compose. Make sure to also remove the dependency on certbot in variable_scraper.

---

## Output

-   The ENVIRONMENT variables are stored in log/data.json
-   The certificates are stored in data/certbot/etc/live (It might be necessary to view them as root)

---

## Description

This application uses a python based script to gather all the ENVIRONMENT variables and store them in a json file. The containers with the VIRTUAL_HOST variable set will automatically get certificates.

---

## Assumptions

-   The certificates are request as staging, since this is a test application.
-   All certificates are always renewed.
-   Port 80 is exposed and forwarded

---

## Testing

A testing docker-compose is provided in the test directory that creates 2 containers with VIRTUAL_HOST and VIRTUAL_PORT set. Any ENVIROMENT variables can be added.